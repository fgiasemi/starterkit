from FunTuple import FunctorCollection, FunTuple_Particles as FunTuple
from DaVinci import Options, make_config
from PyConf.reading import get_particles
import Functors as F

## Specify the stream and stripping line
stream = "AllStreams"
# line   = "D2hhPromptDst2D2KKLine"
line = "Hlt2Charm_DstpToD0Pip_D0ToKmKp"


def main(options: Options):
    data = get_particles("/Event/HLT2/{line}/Particles")

    fields = {
        "Dstar" : "[D*(2010)+ -> (D0 -> K- K+) pi+]CC",
        "D0"    : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC",
        "Kminus": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
        "Kplus" : "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
        "pisoft": "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"
    }
    Dst_variables = FunctorCollection({
        "ID": F.PARTICLE_ID,
        "PT": F.PT,
        "P": F.P,
        "SUMPT": F.SUM(F.PT),
        "MASS": F.MASS,
    })
    other_variables = FunctorCollection({
        "ID": F.PARTICLE_ID,
        "PT": F.PT,
        "P": F.P
    })
    variables = {
        "Dstar": Dst_variables,
        "D0": other_variables,
        "Kminus": other_variables,
        "Kplus": other_variables,
        "pisoft": other_variables,
    }
    evt_variables=FunctorCollection({})
    funtuple = FunTuple(
        name="TupleDstToD0pi_D0ToKK",
        tuple_name="DecayTree",
        inputs=data,
        fields=fields,
        variables=variables,
        event_variables=evt_variables
    )
    user_algorithms = [
        funtuple
    ]
    return make_config(options, user_algorithms)