###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# General imports - to be fill as you add things
from PyConf.components import force_location
from DaVinci.algorithms import add_filter
from DaVinci import make_config
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import Functors as F
from FunTuple.functorcollections import (
    MCHierarchy,
    Kinematics,
    SelectionInfo,
    MCVertexInfo,
    MCKinematics,
    EventInfo,
)
from Functors.math import log

from DaVinci.truth_matching import (
    configured_MCTruthAndBkgCatAlg,
)
from DaVinci.reco_objects import make_pvs_v2
from DaVinci.algorithms import add_filter, get_odin, get_decreports

def make_composite_variables(options, data, add_truth=True):
    v2_pvs = make_pvs_v2(process=options.process)
    variables = (
        FunctorCollection(
            {
                "MAX_PT": F.MAX(F.PT),
                "MIN_PT": F.MIN(F.PT),
                "SUM_PT": F.SUM(F.PT),
                "MAX_P": F.MAX(F.P),
                "MIN_P": F.MIN(F.P),
                "BPVDIRA": F.BPVDIRA(v2_pvs),
                "CHI2VXNDOF": F.CHI2DOF,
                "BPVFDCHI2": F.BPVFDCHI2(v2_pvs),
                "BPVFD": F.BPVFD(v2_pvs),
                "BPVVDRHO": F.BPVVDRHO(v2_pvs),
                "BPVVDZ": F.BPVVDZ(v2_pvs),
                "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
                "BPVIP": F.BPVIP(v2_pvs),
                "LOGBPVIPCHI2": log(F.BPVIPCHI2(v2_pvs)),
                "BPVLTIME": F.BPVLTIME(v2_pvs),
                "MAX_BPVIPCHI2": F.MAX(F.BPVIPCHI2(v2_pvs)),
                "MIN_BPVIPCHI2": F.MIN(F.BPVIPCHI2(v2_pvs)),
                "MAXDOCACHI2": F.MAXDOCACHI2,
                "MAXDOCA": F.MAXDOCA,
                "MAXSDOCACHI2": F.MAXSDOCACHI2,
                "MAXSDOCA": F.MAXSDOCA,
                "MM12": F.SUBCOMB(Functor=F.MASS, Indices=(1, 2)),
                "ETA": F.ETA,
                "PHI": F.PHI,
                "END_VX": F.END_VX,
                "END_VY": F.END_VY,
                "END_VZ": F.END_VZ,
                "BPVX": F.BPVX(v2_pvs),
                "BPVY": F.BPVY(v2_pvs),
                "BPVZ": F.BPVZ(v2_pvs),
            }
        )
        + Kinematics()
    )

    if add_truth:
        variables = add_truth_matching_functors(
            options,
            variables,
            data,
            hierarchy=True,
            kinematics=True,
            vertex_info=True,
            bkgcat=True,
        )

    return variables


def make_basic_variables(options, data, add_truth=True):
    v2_pvs = make_pvs_v2(process=options.process)
    variables = (
        FunctorCollection(
            {
                "TRCHI2DOF": F.CHI2DOF,
                "ETA": F.ETA,
                "PHI": F.PHI,
                "TRGHOSTPROB": F.GHOSTPROB,
                "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
                "BPVIP": F.BPVIP(v2_pvs),
                "PID_K": F.PID_K,
                "PID_E": F.PID_E,
                "PID_MU": F.PID_MU,
                "PID_P": F.PID_P,
                "BPVX": F.BPVX(v2_pvs),
                "BPVY": F.BPVY(v2_pvs),
                "BPVZ": F.BPVZ(v2_pvs),
            }
        )
        + Kinematics()
    )

    if add_truth:
        variables = add_truth_matching_functors(
            options,
            variables,
            data,
            hierarchy=True,
            kinematics=True,
            vertex_info=True,
            bkgcat=False,
        )

    return variables

def make_hlt2_event_variables(options, line_name):
    # get ODIN and DecReports location
    odin = get_odin(options)
    hlt1_dec = get_decreports("Hlt1", options)
    hlt2_dec = get_decreports("Hlt2", options)
    vertices = make_pvs_v2(process=options.process)
    Hlt1_decisions = [
        "Hlt1TwoTrackMVACharmXSecDecision",
        "Hlt1TrackMVADecision",
        "Hlt1TwoTrackMVADecision",
        "Hlt1TwoTrackKsDecision",
        "Hlt1D2KPiDecision",
        "Hlt1GECPassthroughDecision",
        "Hlt1KsToPiPiDecision",
    ]
    Hlt2_decisions = [
        "Hlt2Charm_DstpToD0Pip_D0ToKmPip_XSec_LineDecision",
        "Hlt2Charm_D0ToKmPip_XSec_LineDecision",
    ]
    # define event level variables
    evt_variables = EventInfo(odin, extra_info=True)
    evt_variables += SelectionInfo("Hlt2", hlt2_dec, Hlt2_decisions)
    evt_variables += SelectionInfo("Hlt1", hlt1_dec, Hlt1_decisions)
    evt_variables += FunctorCollection(
        {
            "ALLPVX": F.ALLPVX(vertices),
            "ALLPVY": F.ALLPVY(vertices),
            "ALLPVZ": F.ALLPVZ(vertices),
            "NPV": F.SIZE(vertices),
        }
    )
    return evt_variables


def add_truth_matching_functors(
    options,
    variables,
    data,
    hierarchy=True,
    kinematics=True,
    vertex_info=True,
    bkgcat=False,
):
    """Add MC truth matching functors to an existing FunctorCollection.

    Args:
        options (DaVinci.Options): DaVinci options object.
        variables (FunctorCollection): FunctorCollection to modify
        data: data handle
        hierarchy (bool): Add MCHierarchy info (default True)
        kinematics (bool): Add MCKinematics info (default True)
        vertex_info (bool): Add MCVertexInfo (default True)
        bkgcat (bool): Add TRUEKEY and BKGCAT functors - intended for composite particles (default False)

    Returns:
        FunctorCollection: modified FunctorCollection with truth matched variables.
    """
    mctruth = configured_MCTruthAndBkgCatAlg(inputs=data, process=options.process)
    MCTR = lambda func: F.MAP_INPUT(Functor=func, Relations=mctruth.MCAssocTable)
    trueid_bkgcat_info = {
        # Important note: specify an invalid value for integer functors if there exists no truth info.
        #                 The invalid value for floating point functors is set to nan.
        "TRUEKEY": F.VALUE_OR(-1) @ MCTR(F.OBJECT_KEY),
        "BKGCAT": F.BKGCAT(Relations=mctruth.BkgCatTable),
    }

    if bkgcat:
        variables += FunctorCollection(trueid_bkgcat_info)
    if hierarchy:
        variables += MCHierarchy(mctruth)
    if vertex_info:
        variables += MCVertexInfo(mctruth)
    if kinematics:
        variables += MCKinematics(mctruth)

    return variables

# define the algorithm you want to call 
# (general give it the name of the decay or types of decays)
# as arg it the yaml option file

def D0ToKmPip_alg_config(options):
    line_name = "Hlt2Charm_D0ToKmPip_XSec_Line"

    data = force_location(f"/Event/HLT2/{line_name}/Particles")

    fields={
        "D0": "[D0 -> K- pi+]CC",
        "Km": "[D0 -> ^K- pi+]CC",
        "Pip": "[D0 -> K- ^pi+]CC",
    }
    variables ={
        "D0": make_composite_variables(options, data),
        "Km": make_basic_variables(options, data),
        "Pip": make_basic_variables(options, data),
    }
    event_variables=make_hlt2_event_variables(options, line_name)

    hlt_filter = add_filter(
        options,
        f"HDRFilter_D0ToKmPip",
        f"HLT_PASS('Hlt2Charm_D0ToKmPip_XSec_Line')",
    )

    FunTuple = Funtuple(
            name="D0ToKmPip",
            tuple_name="DecayTree",
            fields=fields,
            variables=variables,
            inputs=data,
            event_variables=event_variables,
        )

    algs = {
        f"Tuple{'D0ToKmPip'}": [hlt_filter, FunTuple]
    }

    return make_config(options, algs)
