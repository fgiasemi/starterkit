###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.charm import all_lines

options.output_manifest_file = "starterkit_hlt2_integration.tck.json"
options.output_file = 'starterkit_hlt2_integration_test.dst'
options.output_type = 'ROOT'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'

def make_lines():
    lines = [builder() for builder in all_lines.values()]
    return lines

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
	config = run_moore(options, make_lines, public_tools)

