from Moore import options, run_allen
from AllenConf.velo_reconstruction import decode_velo

options.set_input_and_conds_from_testfiledb("MiniBrunel_2018_MinBias_FTv4_MDF")

options.output_file = "allen_persistence_dst_write.dst"
options.output_type = 'ROOT'

with decode_velo.bind(retina_decoding=False):
    run_allen(options)
